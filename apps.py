apps = {
    'snap': 'snapd',
    'h264-codecs': ['sudo apt install -y libdvdnav4 libdvd-pkg gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly libdvd-pkg ubuntu-restricted-extras', 'steps'],
    'vscode': ['wget https://az764295.vo.msecnd.net/stable/054a9295330880ed74ceaedda236253b4f39a335/code_1.56.2-1620838498_amd64.deb && sudo dpkg -i code_1.56.2-1620838498_amd64.deb', 'steps'],
    #'telegram': 'telegram-desktop',
    'telegram': ['cd ~ ; mkdir -p Apps ; cd Apps ; wget -O ~/telegram.tar.gz https://telegram.org/dl/desktop/linux ; tar -xf telegram.tar.gz'],
    'wine': 'wine',
    'chrome': 'wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && sudo dpkg -i google-chrome-stable_current_amd64.deb',
    'docker': 'docker docker-compose',
    'qbittorrent': 'qbittorrent',
    'Qcopy': 'sudo add-apt-repository -y ppa:hluk/copyq && sudo apt update & sudo apt install -y copyq',
    'Konqueror': 'konqueror',
    'tmux': 'tmux',
    'mkvtoolnix': 'mkvtoolnix',
    'mkvtoolnix-gui': 'mkvtoolnix-gui',
    'sqlitebrowser': 'sqlitebrowser',
    'midnight-commander': 'mc',  # file explorer in terminal
    'htop': 'htop',  # show resources
    'screenfetch': 'screenfetch',  # show system info
    'vmware' : ['sudo apt-get install gcc build-essential linux-headers-$(uname -r) && wget -O ~/vmware.bin https://download3.vmware.com/software/wkst/file/VMware-Workstation-Full-17.5.2-15785246.x86_64.bundle && sudo bash ~/vmware.bin', 'steps'],  # MC60H-DWHD5-H80U9-6V85M-8280D
    'vmware_tools' : ['VMWARE_VERSION=workstation-17.5.1 && TMP_FOLDER=/tmp/patch-vmware & rm -fdr $TMP_FOLDER & mkdir -p $TMP_FOLDER & cd $TMP_FOLDER & git clone https://github.com/mkubecek/vmware-host-modules.git & cd $TMP_FOLDER/vmware-host-modules & git checkout $VMWARE_VERSION & git fetch & make & sudo make install & sudo rm /usr/lib/vmware/lib/libz.so.1/libz.so.1 & sudo ln -s /lib/x86_64-linux-gnu/libz.so.1 /usr/lib/vmware/lib/libz.so.1/libz.so.1 & sudo /etc/init.d/vmware restart', 'steps'],
    'virtual-box' : 'virtualbox',
    'typora' : ["wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add - & sudo add-apt-repository 'deb https://typora.io/linux ./' & sudo apt-get update & sudo apt-get install -y typora", 'steps'],
    'pyinstaller': ['pyinstaller', 'python3'],
    'colorama': ['colorama', 'python3'],
    'chardet': ['chardet', 'python3'],
    'lxml': ['lxml', 'python3'],
    'beautifulsoup4': ['beautifulsoup4', 'python3'],
    'subliminal': ['subliminal', 'python3'],
    'python3': 'python3-pip',
    'net-tools': 'net-tools',
    'notion': ['cd ~ && mkdir -p Apps && cd Apps && git clone https://github.com/sysdrum/notion-app.git', 'steps'],
    'etcher': ['echo "deb https://dl.bintray.com/resin-io/debian stable etcher" | sudo tee /etc/apt/sources.list.d/etcher.list && sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61 && sudo apt-get update', 'steps'],
    'kodi': 'kodi',
    'teamviewer': ['sudo apt-get install -y libqt5webkit5 & wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb && sudo dpkg -i teamviewer*', 'steps'],
    'gdebi-core' : 'gdebi-core',
    'libudev0' : ['wget http://mirrors.kernel.org/ubuntu/pool/main/u/udev/libudev0_175-0ubuntu9_amd64.deb && sudo dpkg -i libudev0_175-0ubuntu9_amd64.deb', 'steps'],  # avoids error message "error while loading shared libraries: libudev.so.0",  # "dpkg -i" replaces "gdebi"
    'vlc-subtitle-fix' : ['mkdir ~/.cache/vlc', 'steps'],
    # 'OBS': ['sudo apt-get install -y ffmpeg && sudo add-apt-repository -y ppa:obsproject/obs-studio && sudo apt-get update && sudo apt-get -y install obs-studio', 'steps'],
    'OBS': 'obs-studio',
    'Tweaks': 'gnome-tweak-tool',  # sudo add-apt-repository universe
    'Gnome-Extensions': 'gnome-shell-extensions',
    'Colors-folder': ['sudo add-apt-repository ppa:costales/yaru-colors-folder-color & sudo apt install -y folder-color yaru-colors-folder-color ', 'steps'],
    'Pycharm': ['export version=2020.3.1 && wget https://download.jetbrains.com/python/pycharm-community-${version}.tar.gz && tar xvzf pycharm-community-${version}.tar.gz', 'steps'],
    'slack': ['slack-desktop'],

    # snap
    'Microsoft ToDo': ['ao', 'snap'],
    'gitkraken': ['gitkraken', 'snap'],
    'notepad++': ['notepad-plus-plus', 'snap'],
    'sublime': ['sublime-text --classic', 'snap'],
    'youtube-downloader': ['youtube-dl --edge', 'snap'],
    # 'slack': ['slack --classic', 'snap'],
    'skype': ['skype --channel=insider/stable --classic', 'snap'],
    # 'spotify': ['spotify', 'snap'],
    'spotify': ['curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add - && echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list && sudo apt-get update && sudo apt-get install -y spotify-client', 'steps'],
    # 'vlc': ['vlc', 'snap'],
    'vlc': ['sudo add-apt-repository ppa:videolan/master-daily && sudo apt-get update && sudo apt-get install vlc', 'steps'],
    
    # settings
    'update' : ['echo \'alias update="sudo apt update && sudo apt upgrade -y"\' >>  ~/.bashrc', 'steps'],
    'install' : ['echo \'alias install="sudo apt install -y"\' >>  ~/.bashrc', 'steps']
}

