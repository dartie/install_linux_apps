# -*- coding: utf-8 -*-
#
# Author:   Dario Necco
# Company:  Dartie
#
# This script allows to install linux apps from a list
# It handles installation via linux package manager, snap, and command line steps
import locale
import os
import sys
import platform
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
import logging
import inspect
import time
import shutil
import io
import re
import subprocess
from subprocess import Popen, PIPE
from sys import platform as _platform
import traceback
import pprint
from distutils import dir_util
from xml.dom import minidom
import datetime
# import chardet  # uncomment only if necessary, it requires installation
from apps import apps as app_cmds_dict

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

if sys.version[0] == '2':
    from imp import reload

    reload(sys)
    sys.setdefaultencoding("utf-8")

# import Colorama
try:
    from colorama import init, Fore, Back, Style
    init()
except ImportError:
    print('Colorama not imported')

locale.setlocale(locale.LC_ALL, 'C')  # set locale

# set version and author  
__version__ = '1.2'
intern_version = '0003'

# I obtain the app_cmd directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp_cmd = os.path.dirname(sys.executable)
    dirapp_cmd_bundle = sys._MEIPASS
    executable_name = os.path.basename(sys.executable)
else:
    # unfrozen
    dirapp_cmd = os.path.dirname(os.path.realpath(__file__))
    dirapp_cmd_bundle = dirapp_cmd
    executable_name = os.path.basename(__file__)

##############################################################################################
# DEBUG
this_scriptFile = inspect.getfile(inspect.currentframe())
this_scriptFile_filename = os.path.basename(this_scriptFile)
this_scriptFile_filename_noext, ext = os.path.splitext(this_scriptFile_filename)

# logging.basicConfig(filename=this_scriptFile_filename_noext + '.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')  # uncomment for Logging

print('Working dir: \"' + dirapp_cmd + '\"')
welcome_text = '{char1} {app_cmdname} v.{version} {char2}'.format(char1='-' * 5, 
                                                              app_cmdname=os.path.splitext(os.path.basename(__file__))[0], 
                                                              version=__version__, char2='-' * 5)
print(welcome_text)
logging.info(welcome_text)

# global variables (colors)
app_list_color = Fore.MAGENTA + Style.BRIGHT
app_color = Fore.CYAN + Style.BRIGHT
method_color = Fore.CYAN + Style.BRIGHT
warning_color = Fore.YELLOW + Style.BRIGHT
bright_style = Style.BRIGHT
reset_color = Style.RESET_ALL


def get_distro():
    """
    Name of your Linux distro (in lowercase).
    """
    with open("/etc/issue") as f:
        return f.read().lower().split()[0]
        

def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    
    # parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)
    #                     # help='Increase output verbosity. Output files don\'t overwrite original ones'
    # 
    # parser.add_argument("-op", "--output", dest="output_path",
    #                     help="qac output path. it has given in input to this app_cmdlication")
    #                     # action='store_true'
    #                     # nargs="+",
    #                     # nargs='?',  # optional argument
    #                     # default=""
    #                     # type=int
    #                     # choices=[]
    # 
    parser.add_argument('-m', '--mode', default='install', choices=['show', 'install'],
                         help='Select "show" for displaying the list of apps available to be installed')    
    
    parser.add_argument("-a", "--apps", default=[], nargs="+",
                         help="Input of apps to install")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def install_app(app_name, app_cmd):
    print(f'{app_color}\n> {app_name.title()}{reset_color}')
    if isinstance(app_cmd, list):  # not Ubuntu package
        if len(app_cmd) == 2:
            if app_cmd[1] == 'snap':
                cmd = 'sudo snap install {}'.format(app_cmd[0])
            elif app_cmd[1] == 'python3':
                cmd = '{no_confirm}pip3 install {py_pkg}'.format(no_confirm='printf "y" | ', py_pkg=app_cmd[0])
            elif app_cmd[1] == 'steps':
                cmd = app_cmd[0]
            else:
                cmd = app_cmd[0]  # steps
        else:
            cmd = app_cmd[0]  # steps
    else:
        # use linux package manager
        os_info = get_distro().lower()
        if os_info == 'ubuntu':
            cmd = 'sudo apt -y install {}'.format(app_cmd)
        elif 'suse' in os_info:
            cmd = 'zypper -n install {}'.format(app_cmd)
        elif os_info == 'arch':
            cmd = 'pacman -S --noconfirm {}'.format(app_cmd)
        else:
            cmd = 'sudo apt -y install {}'.format(app_cmd)  # ubuntu
    
    os.system(cmd)


def main(args=None):
    if args is None:
        args = check_args()
          
    # update repo
    if args.mode != 'show':
        cmd = 'sudo apt-get update'
        print(f'{app_color} ** UPDATE REPOS ** {reset_color}')
        os.system(cmd)
        print('\n\n')
    
    # print available apps
    os.system('clear')
    print(f'{bright_style}Apps Available {reset_color}')
    for app_name in app_cmds_dict:
        if isinstance(app_cmds_dict[app_name], list) and len(app_cmds_dict[app_name]) > 1:
            method = Style.RESET_ALL + " - " + method_color + app_cmds_dict[app_name][1] if app_cmds_dict[app_name][1] == 'snap' or app_cmds_dict[app_name][1].startswith('python') else ''
        else:
            method = ''
        print(f'{app_list_color}{app_name}{method}')
    print('\n')
    
    if args.mode == 'show':
        sys.exit(0)

    if len(args.apps) != 0:
        for input_app in args.apps:
            if input_app in app_cmds_dict:
                app_cmd = app_cmds_dict[input_app]
                install_app(input_app, app_cmd)
            else:
                print(f'{warning_color}"{input_app}" is not in the list')
    else:
        for app_name, app_cmd in app_cmds_dict.items():
            install_app(app_name, app_cmd)
               
    print(f'\n\n{bright_style}Packages installation completed! Bye')
    

if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
            
# TODO: run sudo systemctl enable --now snapd.socket after snapd installation
# TODO: summarise status of installation with green and red
